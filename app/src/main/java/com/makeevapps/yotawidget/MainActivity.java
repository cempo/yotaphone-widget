package com.makeevapps.yotawidget;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import java.util.Random;

public class MainActivity extends AppCompatActivity {
    public static String QUOTE_INDEX = "quote_index";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        String[] quotes = getResources().getStringArray(R.array.quotes);
        int quoteIndex;

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            quoteIndex = extras.getInt(QUOTE_INDEX);
        } else {
            quoteIndex = new Random().nextInt(quotes.length - 1);
        }

        TextView quoteTextView = (TextView) findViewById(R.id.quoteTextView);
        quoteTextView.setText(quotes[quoteIndex]);
    }
}
