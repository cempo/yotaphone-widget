package com.makeevapps.yotawidget;

import android.app.Activity;
import android.appwidget.AppWidgetManager;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

public class WidgetConfigActivity extends Activity implements OnClickListener {

    private Intent intent = null;

    private int frWidgetId = AppWidgetManager.INVALID_APPWIDGET_ID;
    private int bsWidgetId = AppWidgetManager.INVALID_APPWIDGET_ID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_widget_config);

        Bundle extras = this.getIntent().getExtras();
        if (extras != null) {
            frWidgetId = extras.getInt(AppWidgetManager.EXTRA_APPWIDGET_ID, AppWidgetManager.INVALID_APPWIDGET_ID);

            // Or you could get Identifiers of Widget that is on a front screen and Widget that is on a back screen
            int ids[] = extras.getIntArray(AppWidgetManager.EXTRA_APPWIDGET_IDS);
            if (ids != null) {
                frWidgetId = ids[0]; // Widget that is on a front screen (into YotaHub)
                bsWidgetId = ids[1]; // Widget that is on a back screen
            }

        }

        // Setting up a default result for this activity
        intent = new Intent();
        intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, frWidgetId);
        setResult(RESULT_CANCELED, intent);

        if (frWidgetId != AppWidgetManager.INVALID_APPWIDGET_ID) {
            TextView textView = (TextView) findViewById(R.id.text);
            textView.setText("Front Screen Widget ID: " + frWidgetId + "\nBack Screen Widget ID:" + bsWidgetId);
        } else {
            finish();
        }
    }

    @Override
    public void onClick(View v) {
        setResult(RESULT_OK, intent);
        finish();
    }

}
